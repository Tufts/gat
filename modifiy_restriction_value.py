from pybitbucket import bitbucket as bb
from pybitbucket.auth import BasicAuthenticator
from pybitbucket.branchrestriction import BranchRestriction, BranchRestrictionPayload, BranchRestrictionKind
from pybitbucket.team import Team
from pybitbucket.repository import Repository

import sys
from YamJam import yamjam

# CONFIG
cfg = yamjam()['bb-api']

# Replace this with sys arg: --for-user
TEAM_NAME = "doctorwebbrasil"

# Arguements
branch = sys.argv[1]
kind = sys.argv[2]
value = int(sys.argv[3])

restriction_kind = BranchRestrictionKind(kind)
restriction = BranchRestrictionPayload().add_kind(restriction_kind).add_value(value)

# Authentication
authentication = BasicAuthenticator(username=cfg['user'], password=cfg['password'], client_email=cfg['email'])
bitbucket = bb.Client(authentication)

# Find DoctorWeb tea
team = Team.find_team_by_username(TEAM_NAME, client=bitbucket)
repositories = Repository.find_repositories_by_owner_and_role(owner=team.username, role='member', client=bitbucket)

# Access each repository.
for repos in repositories:
    restrictions = BranchRestriction.find_branchrestrictions_for_repository_by_id(repos.uuid, owner=team.username, client=bitbucket)
    for r in restrictions:
        if isinstance(r, BranchRestriction):
            if r.kind == kind and r.pattern == branch:
                print("%s: Modifying %s restriction for %s branch with value of %d" % (repos.name, kind, branch, value))
                r.modify(restriction)